package com.sebastian.csv;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.sebastian.csv.dto.CarDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class CsvConverter {
    public static String TYPE = "text/csv";

    public static boolean hasCSVFormat(MultipartFile file) {
        return TYPE.equals(file.getContentType());
    }

    public static List<CarDto> csvToCars(InputStream is) {
        List<CarDto> carDtos = new ArrayList<>();

        try {
            BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));

            CSVParser parser = new CSVParserBuilder().withSeparator(',').build();

            CSVReader csvReader = new CSVReaderBuilder(fileReader).withCSVParser(parser).withSkipLines(1).build();

            List<String[]> allData = csvReader.readAll();

            for (String[] csvRecord : allData) {

                CarDto carDto = new CarDto(
                        csvRecord[0],
                        csvRecord[1],
                        Integer.parseInt(csvRecord[2]),
                        csvRecord[3]);

                        carDtos.add(carDto);
            }

        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Failed to parse CSV file: " + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return carDtos;
    }
}
