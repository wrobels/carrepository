package com.sebastian.csv.controller;

import com.sebastian.csv.CsvConverter;
import com.sebastian.csv.dto.CarDto;
import com.sebastian.csv.entity.Car;
import com.sebastian.csv.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/cars")
public class CarController {

    @Autowired
    CarService carService;

    @GetMapping(value = "/all")
    public ResponseEntity<List<Car>> getAllCars() {
        try {
            List<Car> cars = carService.getAllCars();

            if (cars.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(cars, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/brand/{brand}")
    public ResponseEntity<List<Car>> getCarsByBrand(@PathVariable("brand") String brand) {
        try {
            List<Car> cars = carService.getCarsByBrand(brand);

            if (cars.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(cars, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/add")
    public ResponseEntity<String> addCar(@RequestBody CarDto carDto) {
        carService.addCar(carDto);

        return new ResponseEntity<>("Added", HttpStatus.OK);
    }

    @PostMapping(value = "/add/csv")
    public ResponseEntity<String> addCarsFromCsv(@RequestParam("file") MultipartFile file) {
        if (CsvConverter.hasCSVFormat(file)) {
            try {
                carService.addCarsFromCsv(file);

                return new ResponseEntity<>("Added", HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>("Failed to upload csv file", HttpStatus.EXPECTATION_FAILED);
            }
        }
        return new ResponseEntity<>("Missing csv file", HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/id/{id}")
    public ResponseEntity<String> deleteCarById(@PathVariable Long id) {

        carService.deleteCarById(id);

        return new ResponseEntity<>("Deleted", HttpStatus.OK);

    }
}
