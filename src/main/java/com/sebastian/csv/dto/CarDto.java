package com.sebastian.csv.dto;

import com.sebastian.csv.entity.Car;

public class CarDto {
    private final String brand;
    private final String model;
    private final Integer year;
    private final String color;

    public CarDto(String brand, String model, Integer year, String color) {
        this.brand = brand;
        this.model = model;
        this.year = year;
        this.color = color;
    }

    public Car toEntity(String brand, String model, Integer year, String color) {
        return new Car(brand, model, year, color);
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public Integer getYear() {
        return year;
    }

    public String getColor() {
        return color;
    }
}
