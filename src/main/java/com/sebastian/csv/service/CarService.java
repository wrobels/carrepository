package com.sebastian.csv.service;

import com.sebastian.csv.CsvConverter;
import com.sebastian.csv.dto.CarDto;
import com.sebastian.csv.entity.Car;
import com.sebastian.csv.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class CarService {

    @Autowired
    CarRepository carRepository;

    public List<Car> getAllCars() {
        return carRepository.findAll();
    }

    public void addCar(CarDto carDto) {
        Car car = carDto.toEntity(carDto.getBrand(), carDto.getModel(), carDto.getYear(), carDto.getColor());
        carRepository.save(car);
    }

    public void deleteCarById(Long id) {
        carRepository.deleteById(id);
    }

    public void addCarsFromCsv(MultipartFile carsCsv) {
        try {
            List<CarDto> carDtoList = CsvConverter.csvToCars(carsCsv.getInputStream());
            List<Car> cars = new ArrayList<>();
            for (CarDto carDto : carDtoList) {
                Car car = carDto.toEntity(carDto.getBrand(), carDto.getModel(), carDto.getYear(), carDto.getColor());
                cars.add(car);
            }
            carRepository.saveAll(cars);
        } catch (IOException e) {
            throw new RuntimeException("fail to store csv data: " + e.getMessage());
        }
    }

    public List<Car> getCarsByBrand(String brand) {
        return carRepository.findAllByBrand(brand);
    }
}
